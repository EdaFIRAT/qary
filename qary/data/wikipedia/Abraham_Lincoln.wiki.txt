{{About|the 16th president of the United States}}
{{pp|small=yes}}
{{pp-move-indef}}
{{short description|16th president of the United States}}
{{Use American English|date=July 2020}}
{{Use mdy dates|date=April 2020}}
{{Infobox officeholder
| name = Abraham Lincoln
| image = Abraham Lincoln O-77 matte collodion print.jpg
| caption = Lincoln in November 1863
| alt = An iconic photograph of a bearded Abraham Lincoln showing his head and shoulders.
| office = 16th [[President of the United States]]
| vicepresident = [[Hannibal Hamlin]] (1861–65) <br>Andrew Johnson (Mar.–Apr. 1865)
| term_start = March 4, 1861
| term_end = April 15, 1865
| predecessor = [[James Buchanan]]
| successor = [[Andrew Johnson]]
| state1 = [[Illinois]]
| district1 = {{ushr|IL|7|7th}}
| term_start1 = March 4, 1847
| term_end1 = March 3, 1849
| predecessor1 = [[John Henry (representative)|John Henry]]
| successor1 = [[Thomas L. Harris]]
| office2 = Member of the<br />[[Illinois House of Representatives]]<br />from [[Sangamon County]]
| term_start2 = December 1, 1834
| term_end2 = December 4, 1842
| birth_date = {{birth date|1809|2|12}}
| birth_place = [[Sinking Spring Farm]], [[Kentucky]], U.S.
| death_date = {{death date and age|1865|4|15|1809|2|12}}
| death_place = [[Washington, D.C.|Washington County]], U.S.
| death_cause = [[Assassination of Abraham Lincoln|Assassination]] <br>([[gunshot wound]] to the head)
| restingplace = [[Lincoln Tomb]]
| height = 6 ft 4 in<ref name="LincolnHeight">{{cite book|last=Carpenter|first=Francis B.|title=Six Months in the White House: The Story of a Picture|url=https://archive.org/details/sixmonthsatwhit02carpgoog|year=1866|publisher=Hurd and Houghton.|page=[https://archive.org/details/sixmonthsatwhit02carpgoog/page/n225 217]}}</ref>
| party = [[Whig Party (United States)|Whig]] (before 1854)<br />[[Republican Party (United States)|Republican]] (1854–1864)<br />[[National Union Party (United States)|National Union]] (1864–1865)
| spouse = {{marriage|[[Mary Todd Lincoln|Mary Todd]]|November 4, 1842}}
| children = {{hlist|[[Robert Todd Lincoln|Robert]]|[[Edward Baker Lincoln|Edward]]|[[William Wallace Lincoln|Willie]]|[[Tad Lincoln|Tad]]}}
| mother = [[Nancy Hanks Lincoln|Nancy Hanks]]
| father = [[Thomas Lincoln]]
| signature = Abraham Lincoln 1862 signature.svg
| signature_alt = Cursive signature in ink
| allegiance = United States<br />[[Illinois]]
| branch = [[Illinois Militia]]
| serviceyears = 1832
| rank = [[Captain (US Army)|Captain]]{{Efn|name="Ranks"|Discharged from command-rank of Captain and re-enlisted at rank of Private.}}<br />[[Private (United States)|Private]]{{Efn|name="Ranks"}}
| battles = [[American Indian Wars]]<br />
* [[Black Hawk War]]
** [[Battle of Kellogg's Grove]]
** [[Battle of Stillman's Run]]
}}
{{Abraham Lincoln series}}
'''Abraham Lincoln''' ({{IPAc-en|ˈ|l|ɪ|ŋ|k|ən}};<ref>{{Cite web|title=Lincoln &#124; Definition of Lincoln by Merriam-Webster|website=Merriam-Webster|access-date=5 May 2020|url-status=live|url=https://www.merriam-webster.com/dictionary/Lincoln}}</ref> February 12, 1809 – April 15, 1865) was an American statesman and lawyer who served as the 16th [[president of the United States]] from 1861 to 1865. Lincoln led the nation through its greatest moral, constitutional, and political crisis in the [[American Civil War]]. He succeeded in preserving the [[Union (American Civil War)|Union]], [[Thirteenth Amendment to the United States Constitution|abolishing slavery]], bolstering the [[Federal government of the United States|federal government]], and modernizing the [[Economy of the United States|U.S. economy]].
